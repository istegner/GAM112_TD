﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    private bool doMovement = true;

    [Tooltip("If true, escape key will toggle camera movement")]
    public bool debug = false;

    [Tooltip("The speed the camera will pan")]
    public float panSpeed;
    [Tooltip("The speed the camera will scroll")]
    public float scrollSpeed;
    [Tooltip("The multiplication factor the camera applied to the scrollwheel. " +
        "The input from the scrollwheel is a very small number (0.2) so make this a very large number (1000)")]
    public float scrollFactor;
    [Tooltip("How far away from the game border before the camera pans")]
    public float panBorderThickness;
    [Tooltip("The minimum distance the camera will go from the ground")]
    public float minY;
    [Tooltip("The maximum distance the camera will go from the ground")]
    public float maxY;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && debug)
        {
            doMovement = !doMovement;
        }
        if (!doMovement)
        {
            return;
        }
        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            transform.Translate(Vector3.left * panSpeed * Time.deltaTime,Space.World);
        }
        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y <= panBorderThickness)
        {
            transform.Translate(Vector3.right * panSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            transform.Translate(Vector3.forward * panSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x <= panBorderThickness)
        {
            transform.Translate(Vector3.back * panSpeed * Time.deltaTime, Space.World);
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        Vector3 pos = transform.position;
        pos.y -= scroll * scrollFactor * scrollSpeed * Time.deltaTime;
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        transform.position = pos;
    }
}
