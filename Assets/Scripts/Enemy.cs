﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float startSpeed;
    [HideInInspector]
    public float speed;
    public float startHealth;
    public float sensitivity;

    public int deathMoney;

    private float health;

    public GameObject deathEffect;

    [Header("Unity Stuff")]
    public Image healthbar;

    private void Start()
    {
        speed = startSpeed;
        health = startHealth;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;

        healthbar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        PlayerStats.money += deathMoney;
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        WaveSpawner.enemiesAlive--;
        Destroy(gameObject);
    }

    public void Slow(float slowFactor)
    {
        speed = startSpeed * (1f - slowFactor);
    }
}
