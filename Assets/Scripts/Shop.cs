﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standardTurret;
    public TurretBlueprint missileLauncher;
    public TurretBlueprint laserTurret;

    public Text standardTurretText;
    public Text missileLauncherText;
    public Text laserTurretText;

    BuildManager buildManager;

    public void SelectStandardTurret()
    {
        Debug.Log("Standard Turret Selected");
        buildManager.SelectTurretToBuild(standardTurret);
    }

    public void SelectMissileLauncher()
    {
        Debug.Log("Missile Launcher Selected");
        buildManager.SelectTurretToBuild(missileLauncher);
    }

    public void SelectLaserTurret()
    {
        Debug.Log("Laser Turret Selected");
        buildManager.SelectTurretToBuild(laserTurret);
    }

    // Use this for initialization
    void Start()
    {
        buildManager = BuildManager.instance;
        standardTurretText.text = "$" + standardTurret.cost.ToString();
        missileLauncherText.text = "$" + missileLauncher.cost.ToString();
        laserTurretText.text = "$" + laserTurret.cost.ToString();
    }
}
